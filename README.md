# GitLab CI Metrics for Grafana

This is a Grafana backend plugin which provides basic metrics about GitLab CI.
Currently only code coverage and pipeline duration are collected. In future it
is hoped that a variety of metrics will be collected in order to allow  users
to optimise their pipeline flow.