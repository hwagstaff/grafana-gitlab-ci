module github.com/grafana/simple-datasource-backend

go 1.14

require (
	github.com/go-delve/delve v1.4.1 // indirect
	github.com/grafana/grafana-plugin-sdk-go v0.65.0
)
