#!/bin/bash

echo "Building Context"
rm -rf .run
mkdir -p .run

echo " - Copying plugin..."
cp -r dist .run/dist
echo " - Copying provisioning data..."
cp docker/run/*.json .run/
cp docker/run/*.yaml .run/

if [ -f .secrets/gitlab-key ]; then
    export GITLAB_API_TOKEN=`cat .secrets/gitlab-key`
    echo " - Loaded GitLab API token"
    if [ "$GITLAB_API_TOKEN" = "" ]; then
        echo " ? GitLab API token appears empty!"
    fi
else
    echo " X Could not load API token"
    exit 1
fi

echo "Building Image..."
cd .run
IMAGE=`docker build -q -f ../docker/run/Dockerfile .`

echo "Starting Docker"
docker run --rm -p 3000:3000 -e GITLAB_API_TOKEN=$GITLAB_API_TOKEN -t $IMAGE