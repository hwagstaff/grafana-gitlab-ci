package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"github.com/grafana/grafana-plugin-sdk-go/backend/datasource"
	"github.com/grafana/grafana-plugin-sdk-go/backend/instancemgmt"
	"github.com/grafana/grafana-plugin-sdk-go/backend/log"
	"github.com/grafana/grafana-plugin-sdk-go/data"
)

// newDatasource returns datasource.ServeOpts.
func newDatasource() datasource.ServeOpts {
	// creates a instance manager for your plugin. The function passed
	// into `NewInstanceManger` is called when the instance is created
	// for the first time or when a datasource configuration changed.
	im := datasource.NewInstanceManager(newDataSourceInstance)
	ds := &GitLabCIDataSource{
		im: im,
	}

	return datasource.ServeOpts{
		QueryDataHandler:   ds,
		CheckHealthHandler: ds,
	}
}

// GitLabCIDataSource gets CI metrics from GitLab
type GitLabCIDataSource struct {
	// The instance manager can help with lifecycle management
	// of datasource instances in plugins. It's not a requirements
	// but a best practice that we recommend that you follow.
	im instancemgmt.InstanceManager
}

// GitLabCIInstanceSettings is the configuration settings for this plugin

// GitLabCISecrets contains the gitlab API key
type GitLabCISecrets struct {
	ApiKey string `json:"apiKey"`
}

type GitLabCIInstanceSettings struct {
	GitlabInstance string          `json:"gitlabInstance"`
	UseApiKey      bool            `json:"useApiKey"`
	Secrets        GitLabCISecrets // do not unmarshal
}

// QueryData handles multiple queries and returns multiple responses.
// req contains the queries []DataQuery (where each query contains RefID as a unique identifer).
// The QueryDataResponse contains a map of RefID to the response for each query, and each response
// contains Frames ([]*Frame).
func (td *GitLabCIDataSource) QueryData(ctx context.Context, req *backend.QueryDataRequest) (*backend.QueryDataResponse, error) {
	log.DefaultLogger.Info("QueryData", "request", req)

	// create response struct
	response := backend.NewQueryDataResponse()

	// Unpack instance settings
	var settings GitLabCIInstanceSettings
	json.Unmarshal(req.PluginContext.DataSourceInstanceSettings.JSONData, &settings)

	settings.Secrets.ApiKey = req.PluginContext.DataSourceInstanceSettings.DecryptedSecureJSONData["apiKey"]

	// loop over queries and execute them individually.
	for _, q := range req.Queries {
		res := td.query(ctx, settings, q)

		// save the response in a hashmap
		// based on with RefID as identifier
		response.Responses[q.RefID] = res
	}

	return response, nil
}

type queryModel struct {
	Project             string `json:"project"`
	Ref                 string `json:"ref"`
	Field               string `json:"field"`
	IncludeID           bool   `json:"include_id"`
	IncludePipelineLink bool   `json:"include_link"`
}

func (td *GitLabCIDataSource) query(ctx context.Context, settings GitLabCIInstanceSettings, query backend.DataQuery) backend.DataResponse {
	// Unmarshal the json into our queryModel
	var qm queryModel

	response := backend.DataResponse{}

	response.Error = json.Unmarshal(query.JSON, &qm)
	if response.Error != nil {
		return response
	}

	// Log a warning if `Format` is empty.
	if qm.Project == "" {
		log.DefaultLogger.Warn("Project is empty")
		return response
	}
	if qm.Field == "" {
		log.DefaultLogger.Warn("Field is empty")
		return response
	}

	pipelines, err := GetPipelineList(settings, qm, query.TimeRange.From, query.TimeRange.To)
	if err != nil {
		log.DefaultLogger.Warn("Error getting pipelines: " + err.Error())
		response.Error = err
		return response
	}

	// create data frame response - we always include a time series, ID series, and query data series
	frame := data.NewFrame("response")
	timeData := make([]time.Time, 0)
	idData := make([]int32, 0)
	fieldData := PipelineQueryField(qm.Field)
	for _, d := range pipelines {
		// not all pipelines can service all queries (e.g. missing coverage data). only add a pipeline
		// to the series if it can service the given query
		queryData := d.QueryPipeline(qm.Field)

		if queryData != nil {
			timeData = append(timeData, queryData.TimeFinished)
			idData = append(idData, int32(queryData.ID))
			fieldData = PipelineQueryFieldAppend(fieldData, qm.Field, queryData.QueryData)
		}
	}

	// Add the two default fields (time and ID)
	frame.Fields = append(frame.Fields, data.NewField("time", nil, timeData))

	if qm.IncludeID {
		frame.Fields = append(frame.Fields, data.NewField("id", nil, idData))
	}

	// Create a field config to be used across the returned fields
	var fieldConf data.FieldConfig

	if qm.IncludePipelineLink {
		// create a data link to allow the user to click a value to be taken straight to the relevant
		// pipeline page on GitLab
		var pipelineLink data.DataLink
		pipelineLink.Title = "Pipeline"
		pipelineLink.TargetBlank = true
		pipelineLink.URL = settings.GitlabInstance + "/" + qm.Project + "/-/pipelines/${__data.fields[id]}"
		fieldConf.Links = append(fieldConf.Links, pipelineLink)
	}

	// Create the query data field
	queryField := data.NewField(qm.Field, nil, fieldData)
	queryField.Config = &fieldConf
	frame.Fields = append(frame.Fields, queryField)

	response.Frames = append(response.Frames, frame)

	return response
}

// CheckHealth handles health checks sent from Grafana to the plugin.
// The main use case for these health checks is the test button on the
// datasource configuration page which allows users to verify that
// a datasource is working as expected.
func (td *GitLabCIDataSource) CheckHealth(ctx context.Context, req *backend.CheckHealthRequest) (*backend.CheckHealthResult, error) {
	var result backend.CheckHealthResult
	var err error
	// Unpack instance settings
	var settings GitLabCIInstanceSettings
	log.DefaultLogger.Info("Checking plugin health")
	json.Unmarshal(req.PluginContext.DataSourceInstanceSettings.JSONData, &settings)

	// First just try to reach gitlab
	var requestUrl = settings.GitlabInstance + "/api/v4/projects/"
	_, err = http.Get(requestUrl)
	if err != nil {
		log.DefaultLogger.Info("Couldn't contact gitlab")
		result.Status = backend.HealthStatusError
		result.Message = "Could not contact Gitlab\n"
		return &result, nil
	} else {
		log.DefaultLogger.Info("Contact OK")
		result.Status = backend.HealthStatusOk
		result.Message = "Gitlab Contact Successful\n"
	}

	// If an API key is provided then test it out
	if !settings.UseApiKey {
		log.DefaultLogger.Info("No API key, so done")
		return &result, nil
	}

	var apiKey, _ = req.PluginContext.DataSourceInstanceSettings.DecryptedSecureJSONData["apiKey"]
	log.DefaultLogger.Info("checking API Key")
	requestUrl = settings.GitlabInstance + "/api/v4/projects?private_token=" + apiKey
	apiKeyTestReq, err := http.Get(requestUrl)
	if err != nil {
		log.DefaultLogger.Info("Could not make request with API key")
		result.Status = backend.HealthStatusError
		result.Message = "API key test error\n"
		return &result, nil
	}

	var apiKeyTestBody []byte
	apiKeyTestBody, err = ioutil.ReadAll(apiKeyTestReq.Body)
	if err != nil {
		log.DefaultLogger.Info("Could not read body data")
		result.Status = backend.HealthStatusError
		result.Message = "Failed to read body data\n"
		return &result, nil
	}

	var data interface{}
	err = json.Unmarshal(apiKeyTestBody, &data)
	if err != nil {
		log.DefaultLogger.Info("Could not unmarshall json")
		result.Status = backend.HealthStatusError
		result.Message = "Failed to unmarshall JSON"
		return &result, nil
	}

	var m = data.(map[string]interface{})
	var apimessage, apimessageexists = m["message"]
	if apimessageexists && apimessage.(string) == "401 Unauthorized" {
		log.DefaultLogger.Info("API key failure")
		result.Status = backend.HealthStatusError
		result.Message = "API Key test failed."
		return &result, nil
	}
	log.DefaultLogger.Info("All done.")

	return &result, nil
}

type instanceSettings struct {
	httpClient *http.Client
}

func newDataSourceInstance(setting backend.DataSourceInstanceSettings) (instancemgmt.Instance, error) {
	return &instanceSettings{
		httpClient: &http.Client{},
	}, nil
}

func (s *instanceSettings) Dispose() {
	// Called before creatinga a new instance to allow plugin authors
	// to cleanup.
}
