package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"time"

	"github.com/grafana/grafana-plugin-sdk-go/backend/log"
)

var iso8601Spec string = "2006-01-02T15:04:05Z0700"

const (
	// PipelineDuration gets the real time duration of a pipeline
	PipelineDuration = "duration"
	// PipelineQueuedFor gets the number of seconds a pipeline was in the queue
	PipelineQueuedFor = "queue_time"
	// PipelineCoverage gets the code coverage of a pipeline
	PipelineCoverage = "coverage"
)

// PipelineData contains the data for a single gitlab pipeline run
type PipelineData struct {
	PipelineID   int64
	Coverage     *float32
	TimeStarted  time.Time
	TimeFinished time.Time
	TimeCreated  time.Time
	Status       string
}

// PipelineListEntry is the short-form pipeline data returned by gitlab when just asking for 'pipelines'
// rather than a specific pipeline
type PipelineListEntry struct {
	PipelineID int64 `json:"id"`
}

// PipelineQueryResult is the result of calling QueryPipeline
type PipelineQueryResult struct {
	ID           int64
	TimeFinished time.Time
	QueryData    interface{}
}

// PipelineQueryField returns an array suitably typed for the given field
func PipelineQueryField(field string) interface{} {
	switch field {
	case PipelineDuration:
		return make([]float64, 0)
	case PipelineQueuedFor:
		return make([]float64, 0)
	case PipelineCoverage:
		return make([]float32, 0)
	default:
		log.DefaultLogger.Warn("Unrecognized field '" + field + "'")
		return nil
	}
}

// PipelineQueryFieldAppend appends a queried field value to the end of a value array
func PipelineQueryFieldAppend(input interface{}, field string, data interface{}) interface{} {
	switch field {
	case PipelineQueuedFor:
		inputCast := input.([]float64)
		return append(inputCast, data.(float64))
	case PipelineDuration:
		inputCast := input.([]float64)
		return append(inputCast, data.(float64))
	case PipelineCoverage:
		inputCast := input.([]float32)
		return append(inputCast, data.(float32))
	default:
		return nil
	}
}

// QueryPipeline gets a particular query field from a pipeline
func (d *PipelineData) QueryPipeline(field string) *PipelineQueryResult {
	var result PipelineQueryResult
	result.ID = d.PipelineID
	result.TimeFinished = d.TimeFinished

	switch field {
	case PipelineQueuedFor:
		result.QueryData = d.TimeStarted.Sub(d.TimeCreated).Seconds()
	case PipelineDuration:
		result.QueryData = d.TimeFinished.Sub(d.TimeStarted).Seconds()
	case PipelineCoverage:
		if d.Coverage == nil {
			return nil
		}
		result.QueryData = *d.Coverage
	default:
		log.DefaultLogger.Warn("Unrecognized field '" + field + "'")
		return nil
	}
	return &result
}

// UnmarshalJSON performs custom unmarshalling for pipeline data received from Gitlab. In particular
// we want to correctly read the timestamps and code coverage.
func (d *PipelineData) UnmarshalJSON(data []byte) error {
	var v map[string]interface{}
	var err error
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}

	d.PipelineID = int64(v["id"].(float64))
	if cov, ok := v["coverage"]; ok {
		// coverage might be nil or a string
		if covstring, ok := cov.(string); ok {
			parsedFloat, _ := strconv.ParseFloat(covstring, 32)
			castFloat := float32(parsedFloat)
			d.Coverage = &castFloat
		}
	}
	d.TimeStarted, err = TimeFromGitLab(v["started_at"].(string))
	if err != nil {
		return err
	}
	d.TimeFinished, err = TimeFromGitLab(v["finished_at"].(string))
	if err != nil {
		return err
	}
	d.TimeCreated, err = TimeFromGitLab(v["created_at"].(string))
	if err != nil {
		return err
	}

	d.Status = v["status"].(string)

	return nil
}

// TimeFromGitLab converts a time from GitLab JSON to a Go Time struct
func TimeFromGitLab(iso8601 string) (time.Time, error) {
	return time.Parse(iso8601Spec, iso8601)
}

// TimeToGitLab converts a Time to a GitLab JSON compatible string
func TimeToGitLab(time time.Time) string {
	return time.Format(iso8601Spec)
}

// MakeQuery makes an HTTP query to the GitLab instance with the given project and path. It returns
// the body data
func MakeQuery(settings GitLabCIInstanceSettings, project string, resource string, parameters map[string]string) (result []byte, headers http.Header, err error) {
	requestURL := settings.GitlabInstance + "/api/v4/projects/" + project + "/" + resource

	if len(parameters) > 0 {
		requestURL = requestURL + "?"
		first := true
		for k, v := range parameters {
			if !first {
				requestURL += "&"
			}
			requestURL += k + "=" + v
			first = false
		}
	}

	if settings.UseApiKey {
		if len(parameters) > 0 {
			requestURL += "&"
		} else {
			requestURL += "?"
		}
		requestURL += "private_token=" + settings.Secrets.ApiKey
	}

	request, err := http.Get(requestURL)
	if err != nil {
		log.DefaultLogger.Warn("Error fetching pipeline list")
		return
	}
	defer request.Body.Close()

	result, err = ioutil.ReadAll(request.Body)
	if err != nil {
		log.DefaultLogger.Warn("Error getting pipeline list data")
		return
	}

	// Check for error status codes
	if request.StatusCode > 400 {
		log.DefaultLogger.Error("HTTP Error in request \"" + requestURL + "\": " + request.Status)
		return nil, nil, errors.New(request.Status)
	}

	headers = request.Header
	return
}

// GetPipeline fetches a single gitlab pipeline by project and pipeline ID
func GetPipeline(settings GitLabCIInstanceSettings, query queryModel, pipelineID int64) (result PipelineData, err error) {
	result.PipelineID = pipelineID

	body, _, err := MakeQuery(settings, query.Project, "/pipelines/"+strconv.FormatInt(pipelineID, 10), map[string]string{})
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		log.DefaultLogger.Warn("Error while unmarshalling pipeline data: " + err.Error())
		return
	}
	return
}

// getPipelineListInner is the implementation for GetPipelineList. It automatically handles pagination
// and dispatches requests for individual pipelines in parallel.
func getPipelineListInner(settings GitLabCIInstanceSettings, query queryModel, begin, end time.Time, page int) (result []PipelineData, err error) {
	// Format our query
	log.DefaultLogger.Info("Getting pipeline list page")

	params := make(map[string]string)
	if query.Ref != "" {
		params["ref"] = query.Ref
	}
	params["updated_after"] = TimeToGitLab(begin.UTC())
	params["updated_before"] = TimeToGitLab(end.UTC())
	params["page"] = strconv.Itoa(page)

	body, headers, err := MakeQuery(settings, query.Project, "pipelines", params)
	if err != nil {
		return
	}

	var shortList []PipelineListEntry
	err = json.Unmarshal(body, &shortList)
	if err != nil {
		log.DefaultLogger.Warn("Error while unmarshalling pipeline list data: " + err.Error())
		log.DefaultLogger.Warn("List data: " + string(body))
		return
	}

	type channelData struct {
		data PipelineData
		err  error
	}

	channel := make(chan *channelData)
	lambda := func(pipelineID int64) {
		pipelineData, err := GetPipeline(settings, query, pipelineID)
		channel <- &channelData{pipelineData, err}
	}

	for _, e := range shortList {
		go lambda(e.PipelineID)
	}
	for range shortList {
		chData := <-channel
		if chData.err != nil {
			return result, err
		}
		result = append(result, chData.data)
	}

	nextPage := headers.Get("x-next-page")
	if nextPage != "" {
		nextPageInt, _ := strconv.ParseInt(nextPage, 10, 32)
		nextPageList, err := getPipelineListInner(settings, query, begin, end, int(nextPageInt))
		if err != nil {
			return result, err
		}
		result = append(result, nextPageList...)
	}

	return
}

// GetPipelineList gets the pipeline list-entries for the given project between start and end
func GetPipelineList(settings GitLabCIInstanceSettings, query queryModel, begin, end time.Time) (result []PipelineData, err error) {
	pipelines, err := getPipelineListInner(settings, query, begin, end, 1)

	if err != nil {
		return pipelines, err
	}

	// Sort data by pipeline finished time
	sort.Slice(pipelines, func(i, j int) bool {
		iTime := pipelines[i].TimeFinished
		jTime := pipelines[j].TimeFinished

		return iTime.Before(jTime)
	})

	return pipelines, nil
}
