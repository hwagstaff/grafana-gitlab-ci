import { DataSourceInstanceSettings } from '@grafana/data';
import { DataSourceWithBackend } from '@grafana/runtime';
import { GitlabCIDataSourceOptions, GitlabCIQuery } from './types';

export class DataSource extends DataSourceWithBackend<GitlabCIQuery, GitlabCIDataSourceOptions> {
  constructor(instanceSettings: DataSourceInstanceSettings<GitlabCIDataSourceOptions>) {
    super(instanceSettings);
  }
}
