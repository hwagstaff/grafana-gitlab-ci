import defaults from 'lodash/defaults';

import React, { ChangeEvent, PureComponent } from 'react';
import { LegacyForms } from '@grafana/ui';
import { QueryEditorProps } from '@grafana/data';
import { DataSource } from './DataSource';
import { defaultQuery, GitlabCIDataSourceOptions, GitlabCIQuery } from './types';

const { FormField } = LegacyForms;

type Props = QueryEditorProps<DataSource, GitlabCIQuery, GitlabCIDataSourceOptions>;

export class QueryEditor extends PureComponent<Props> {
  onProjectChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, project: event.target.value });
  };
  onRefChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, ref: event.target.value });
  };
  onFieldChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, field: event.target.value });
  };
  onIncludeIdChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, include_id: event.target.checked });
  };
  onIncludeLinkChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, include_link: event.target.checked });
  };

  render() {
    const query = defaults(this.props.query, defaultQuery);
    const { project, ref, field, include_id, include_link } = query;

    return (
      <div className="gf-form">
        <div className="query-editor-row">
          <FormField width={2} value={project} onChange={this.onProjectChange} label="Project ID" type="string" />
          <FormField
            width={2}
            value={ref}
            onChange={this.onRefChange}
            label="Ref"
            type="string"
            placeholder="Leave blank for all"
          />
          <div className="form-field">
            <label className="gf-form-label width-6">Query Field</label>
            <select value={field} onChange={this.onFieldChange} placeholder="Query Field">
              <option value="duration">Duration</option>
              <option value="coverage">Coverage</option>
              <option value="queue_time">Time Queued</option>
            </select>
          </div>
        </div>
        <div>
          <FormField
            width={1}
            checked={include_id}
            onChange={this.onIncludeIdChange}
            label="Include ID"
            type="checkbox"
          />
          <FormField
            width={1}
            checked={include_link}
            onChange={this.onIncludeLinkChange}
            label="Include Link"
            type="checkbox"
          />
        </div>
      </div>
    );
  }
}
