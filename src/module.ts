import { DataSourcePlugin } from '@grafana/data';
import { DataSource } from './DataSource';
import { ConfigEditor } from './ConfigEditor';
import { QueryEditor } from './QueryEditor';
import { GitlabCIQuery, GitlabCIDataSourceOptions } from './types';

export const plugin = new DataSourcePlugin<DataSource, GitlabCIQuery, GitlabCIDataSourceOptions>(DataSource)
  .setConfigEditor(ConfigEditor)
  .setQueryEditor(QueryEditor);
