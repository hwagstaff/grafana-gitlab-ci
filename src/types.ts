import { DataQuery, DataSourceJsonData } from '@grafana/data';

export interface GitlabCIQuery extends DataQuery {
  project?: string;
  ref?: string;
  field?: string;
  include_id?: boolean;
  include_link?: boolean;
}

export const defaultQuery: Partial<GitlabCIQuery> = {};

/**
 * These are options configured for each DataSource instance
 */
export interface GitlabCIDataSourceOptions extends DataSourceJsonData {
  gitlabInstance?: string;
  useApiKey?: boolean;
}

/**
 * Value that is used in the backend, but never sent over HTTP to the frontend
 */
export interface GitlabCISecureData {
  apiKey?: string;
}
